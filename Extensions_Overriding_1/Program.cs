﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Extensions_Overriding_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaring instances of each class

            juice_bottle temp1 = new juice_bottle();
            water_bottle temp2 = new water_bottle();
            water_bottle_2 temp3 = new water_bottle_2();

            //calling all the methods from within each object

            temp1.show_message();
            temp1.extend_juice_bottle_method();
            temp2.show_message();
            temp2.show_message_2();
            temp3.show_message();
            temp3.show_message_2();

            //this is to stop the console output window from vanishing

            Console.ReadLine();

        }
    }


    //here, I have managed to seal this class
    //that means, I cannot derive any classes from this
    public sealed class juice_bottle
    {
        public void show_message()
        {
            Console.WriteLine("This is from the juice bottle");
        }
    }


    public static class juice_bottle_extensions
    {
        //this method is now officially part of the class juice_bottle
        public static void extend_juice_bottle_method(this juice_bottle temp)
        {
            Console.WriteLine("This is from the juice bottle extend_juice_bottle_method method");
        }
    }

    //the following will give a compile error
    //as I am trying to derive a sealed class
    //class juice_bottle_2 : juice_bottle


    class water_bottle
    {
        //I want to allow this method to be overriden by the derived class
        //so I am making it virtual
        public virtual void show_message()
        {
            Console.WriteLine("This is from the water bottle. this may be overridden as it is virtual");
        }

        public void show_message_2()
        {
            Console.WriteLine("This is from the water bottle. this may note be overridden as it is not virtual");
        }

    }

    //I am able to derive from water_bottle because water_bottle is not sealed

    class water_bottle_2 : water_bottle
    {
        //now, I will go ahead and override the show_message method as well
        //for that the override keyword is used and further method in the base class 
        public override void show_message()
        {
            Console.WriteLine("This is from the water bottle 2. ");
            //if I want, I can also call the show_message() that belongs to the base class by using the base keyword
            base.show_message();
            base.show_message_2();
        }

        //the following will give an error
        //this is because, the same method is not marked virtual in the base class definition
        //public override void show_message_2()


        
    }
}
